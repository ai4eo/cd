# -*- coding: utf-8 -*-
"""
Created on Tue Dec 22 20:44:24 2020

@author: Sudipan
"""

# Imports


import sys
# PyTorch
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
from torch.autograd import Variable
import torchvision.transforms as tr
import torch.optim as optim

# Other
import os
import numpy as np
import random
from skimage import io
import matplotlib.pyplot as plt
import scipy.stats as stats

from tqdm import tqdm as tqdm
from pandas import read_csv
from math import floor, ceil, sqrt, exp
from IPython import display
import time
from itertools import chain
import time
import warnings
from pprint import pprint

# Models
from .unet import Unet
from .siamunet_conc import SiamUnet_conc
from .siamunet_diff import SiamUnet_diff
from .fresunet import FresUNet,FresUNetConfidenceAnalysis
from .utilityFunctions import read_sentinel_img_trio,reshape_for_torch,count_parameters,kappa


# Global Variables' Definitions

#PATH_TO_DATASET = 'C:/Users/Sudipan/Desktop/codes/oscdSupervised/OSCDDataset/'
PATH_TO_DATASET = './OSCDDataset/'
IS_PROTOTYPE = False

FP_MODIFIER = 10 # Tuning parameter, use 1 if unsure

BATCH_SIZE = 32
PATCH_SIDE = 96
N_EPOCHS = 50

NORMALISE_IMGS = True

TRAIN_STRIDE = int(PATCH_SIDE/2) - 1

TYPE = 1 # 0-RGB | 1-RGBIr | 2-All bands s.t. resulution <= 20m | 3-All bands

LOAD_TRAINED = True

DATA_AUG = True







class ChangeDetectionDataset(Dataset):
    """Change Detection dataset class, used for both training and test data."""

    def __init__(self, path, typeTrainTestVal, patch_side = 96, stride = None, use_all_bands = False, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        
        # basics
        self.transform = transform
        self.path = path
        self.patch_side = patch_side
        if not stride:
            self.stride = 1
        else:
            self.stride = stride
        
        if typeTrainTestVal=='train':
            fname = 'train.txt'
        elif typeTrainTestVal=='test':
            fname = 'test.txt'
        elif typeTrainTestVal=='validation':
            fname = 'validation.txt'
        else:
            sys.exit('Invalid choice for loading dataset, must be from train/test/validation')
        
#         print(path + fname)
        self.names = read_csv(path + fname).columns
        self.n_imgs = self.names.shape[0]
        
        n_pix = 0
        true_pix = 0
        
        
        # load images
        self.imgs_1 = {}
        self.imgs_2 = {}
        self.change_maps = {}
        self.n_patches_per_image = {}
        self.n_patches = 0
        self.patch_coords = []
        for im_name in tqdm(self.names):
            # load and store each image
            I1, I2, cm = read_sentinel_img_trio(self.path + im_name, TYPE, NORMALISE_IMGS)
            self.imgs_1[im_name] = reshape_for_torch(I1)
            self.imgs_2[im_name] = reshape_for_torch(I2)
            self.change_maps[im_name] = cm
            
            s = cm.shape
            n_pix += np.prod(s)
            true_pix += cm.sum()
            
            # calculate the number of patches
            s = self.imgs_1[im_name].shape
            n1 = ceil((s[1] - self.patch_side + 1) / self.stride)
            n2 = ceil((s[2] - self.patch_side + 1) / self.stride)
            n_patches_i = n1 * n2
            self.n_patches_per_image[im_name] = n_patches_i
            self.n_patches += n_patches_i
            
            # generate path coordinates
            for i in range(n1):
                for j in range(n2):
                    # coordinates in (x1, x2, y1, y2)
                    current_patch_coords = (im_name, 
                                    [self.stride*i, self.stride*i + self.patch_side, self.stride*j, self.stride*j + self.patch_side],
                                    [self.stride*(i + 1), self.stride*(j + 1)])
                    self.patch_coords.append(current_patch_coords)
                    
        self.weights = [ FP_MODIFIER * 2 * true_pix / n_pix, 2 * (n_pix - true_pix) / n_pix]
        
        

    def get_img(self, im_name):
        return self.imgs_1[im_name], self.imgs_2[im_name], self.change_maps[im_name]

    def __len__(self):
        return self.n_patches

    def __getitem__(self, idx):
        current_patch_coords = self.patch_coords[idx]
        im_name = current_patch_coords[0]
        limits = current_patch_coords[1]
        centre = current_patch_coords[2]
        
        I1 = self.imgs_1[im_name][:, limits[0]:limits[1], limits[2]:limits[3]]
        I2 = self.imgs_2[im_name][:, limits[0]:limits[1], limits[2]:limits[3]]
        
        label = self.change_maps[im_name][limits[0]:limits[1], limits[2]:limits[3]]
        label = torch.from_numpy(1*np.array(label)).float()
        
        sample = {'I1': I1, 'I2': I2, 'label': label}
        
        if self.transform:
            sample = self.transform(sample)

        return sample



class RandomFlip(object):
    """Flip randomly the images in a sample."""

#     def __init__(self):
#         return

    def __call__(self, sample):
        I1, I2, label = sample['I1'], sample['I2'], sample['label']
        
        if random.random() > 0.5:
            I1 =  I1.numpy()[:,:,::-1].copy()
            I1 = torch.from_numpy(I1)
            I2 =  I2.numpy()[:,:,::-1].copy()
            I2 = torch.from_numpy(I2)
            label =  label.numpy()[:,::-1].copy()
            label = torch.from_numpy(label)

        return {'I1': I1, 'I2': I2, 'label': label}



class RandomRot(object):
    """Rotate randomly the images in a sample."""

#     def __init__(self):
#         return

    def __call__(self, sample):
        I1, I2, label = sample['I1'], sample['I2'], sample['label']
        
        n = random.randint(0, 3)
        if n:
            I1 =  sample['I1'].numpy()
            I1 = np.rot90(I1, n, axes=(1, 2)).copy()
            I1 = torch.from_numpy(I1)
            I2 =  sample['I2'].numpy()
            I2 = np.rot90(I2, n, axes=(1, 2)).copy()
            I2 = torch.from_numpy(I2)
            label =  sample['label'].numpy()
            label = np.rot90(label, n, axes=(0, 1)).copy()
            label = torch.from_numpy(label)

        return {'I1': I1, 'I2': I2, 'label': label}
    
    


def train(n_epochs, save = True):
    t = np.linspace(1, n_epochs, n_epochs)
    
    epoch_train_loss = 0 * t
    epoch_train_accuracy = 0 * t
    epoch_train_change_accuracy = 0 * t
    epoch_train_nochange_accuracy = 0 * t
    epoch_train_precision = 0 * t
    epoch_train_recall = 0 * t
    epoch_train_Fmeasure = 0 * t
    epoch_validation_loss = 0 * t
    epoch_validation_accuracy = 0 * t
    epoch_validation_change_accuracy = 0 * t
    epoch_validation_nochange_accuracy = 0 * t
    epoch_validation_precision = 0 * t
    epoch_validation_recall = 0 * t
    epoch_validation_Fmeasure = 0 * t
    
#     mean_acc = 0
#     best_mean_acc = 0
    fm = 0
    best_fm = 0
    
    lss = 1000
    best_lss = 1000
    
    plt.figure(num=1)
    plt.figure(num=2)
    plt.figure(num=3)
    
    
    optimizer = torch.optim.Adam(net.parameters(), weight_decay=1e-4)
#     optimizer = torch.optim.Adam(net.parameters(), lr=0.0005)
        
    
    scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, 0.95)
    
    
    for epoch_index in tqdm(range(n_epochs)):
        net.train()
        print('Epoch: ' + str(epoch_index + 1) + ' of ' + str(N_EPOCHS))

        
#         for batch_index, batch in enumerate(tqdm(data_loader)):
        for batch in train_loader:
            I1 = Variable(batch['I1'].float().cuda())
            I2 = Variable(batch['I2'].float().cuda())
            label = torch.squeeze(Variable(batch['label'].cuda()))

            optimizer.zero_grad()
            output = net(I1, I2)
            loss = criterion(output, label.long())
            loss.backward()
            optimizer.step()
            
        scheduler.step()


        epoch_train_loss[epoch_index], epoch_train_accuracy[epoch_index], cl_acc, pr_rec = validationFromTrain(train_dataset)
        
    
    
    
        epoch_train_loss[epoch_index], epoch_train_accuracy[epoch_index], cl_acc, pr_rec = validationFromTrain(train_dataset)
        epoch_train_nochange_accuracy[epoch_index] = cl_acc[0]
        epoch_train_change_accuracy[epoch_index] = cl_acc[1]
        epoch_train_precision[epoch_index] = pr_rec[0]
        epoch_train_recall[epoch_index] = pr_rec[1]
        epoch_train_Fmeasure[epoch_index] = pr_rec[2]
        

        epoch_validation_loss[epoch_index], epoch_validation_accuracy[epoch_index], cl_acc, pr_rec = validationFromTrain(validation_dataset)
        epoch_validation_nochange_accuracy[epoch_index] = cl_acc[0]
        epoch_validation_change_accuracy[epoch_index] = cl_acc[1]
        epoch_validation_precision[epoch_index] = pr_rec[0]
        epoch_validation_recall[epoch_index] = pr_rec[1]
        epoch_validation_Fmeasure[epoch_index] = pr_rec[2]

        plt.figure(num=1)
        plt.clf()
        l1_1, = plt.plot(t[:epoch_index + 1], epoch_train_loss[:epoch_index + 1], label='Train loss')
        l1_2, = plt.plot(t[:epoch_index + 1], epoch_validation_loss[:epoch_index + 1], label='validation loss')
        plt.legend(handles=[l1_1, l1_2])
        plt.grid()
#         plt.gcf().gca().set_ylim(bottom = 0)
        plt.gcf().gca().set_xlim(left = 0)
        plt.title('Loss')
        display.clear_output(wait=True)
        display.display(plt.gcf())

        plt.figure(num=2)
        plt.clf()
        l2_1, = plt.plot(t[:epoch_index + 1], epoch_train_accuracy[:epoch_index + 1], label='Train accuracy')
        l2_2, = plt.plot(t[:epoch_index + 1], epoch_validation_accuracy[:epoch_index + 1], label='validation accuracy')
        plt.legend(handles=[l2_1, l2_2])
        plt.grid()
        plt.gcf().gca().set_ylim(0, 100)
#         plt.gcf().gca().set_ylim(bottom = 0)
#         plt.gcf().gca().set_xlim(left = 0)
        plt.title('Accuracy')
        display.clear_output(wait=True)
        display.display(plt.gcf())

        plt.figure(num=3)
        plt.clf()
        l3_1, = plt.plot(t[:epoch_index + 1], epoch_train_nochange_accuracy[:epoch_index + 1], label='Train accuracy: no change')
        l3_2, = plt.plot(t[:epoch_index + 1], epoch_train_change_accuracy[:epoch_index + 1], label='Train accuracy: change')
        l3_3, = plt.plot(t[:epoch_index + 1], epoch_validation_nochange_accuracy[:epoch_index + 1], label='validation accuracy: no change')
        l3_4, = plt.plot(t[:epoch_index + 1], epoch_validation_change_accuracy[:epoch_index + 1], label='validation accuracy: change')
        plt.legend(handles=[l3_1, l3_2, l3_3, l3_4])
        plt.grid()
        plt.gcf().gca().set_ylim(0, 100)
#         plt.gcf().gca().set_ylim(bottom = 0)
#         plt.gcf().gca().set_xlim(left = 0)
        plt.title('Accuracy per class')
        display.clear_output(wait=True)
        display.display(plt.gcf())

        plt.figure(num=4)
        plt.clf()
        l4_1, = plt.plot(t[:epoch_index + 1], epoch_train_precision[:epoch_index + 1], label='Train precision')
        l4_2, = plt.plot(t[:epoch_index + 1], epoch_train_recall[:epoch_index + 1], label='Train recall')
        l4_3, = plt.plot(t[:epoch_index + 1], epoch_train_Fmeasure[:epoch_index + 1], label='Train Dice/F1')
        l4_4, = plt.plot(t[:epoch_index + 1], epoch_validation_precision[:epoch_index + 1], label='validation precision')
        l4_5, = plt.plot(t[:epoch_index + 1], epoch_validation_recall[:epoch_index + 1], label='validation recall')
        l4_6, = plt.plot(t[:epoch_index + 1], epoch_validation_Fmeasure[:epoch_index + 1], label='validation Dice/F1')
        plt.legend(handles=[l4_1, l4_2, l4_3, l4_4, l4_5, l4_6])
        plt.grid()
        plt.gcf().gca().set_ylim(0, 1)
#         plt.gcf().gca().set_ylim(bottom = 0)
#         plt.gcf().gca().set_xlim(left = 0)
        plt.title('Precision, Recall and F-measure')
        display.clear_output(wait=True)
        display.display(plt.gcf())
        
        
#         mean_acc = (epoch_validation_nochange_accuracy[epoch_index] + epoch_validation_change_accuracy[epoch_index])/2
#         if mean_acc > best_mean_acc:
#             best_mean_acc = mean_acc
#             save_str = 'net-best_epoch-' + str(epoch_index + 1) + '_acc-' + str(mean_acc) + '.pth.tar'
#             torch.save(net.state_dict(), save_str)
        
        
#         fm = pr_rec[2]
        fm = epoch_train_Fmeasure[epoch_index]
        if fm > best_fm:
            best_fm = fm
            # save_str = 'net-best_epoch-' + str(epoch_index + 1) + '_fm-' + str(fm) + '.pth.tar'
            # torch.save(net.state_dict(), save_str)
        
        lss = epoch_train_loss[epoch_index]
        if lss < best_lss:
            best_lss = lss
            # save_str = 'net-best_epoch-' + str(epoch_index + 1) + '_loss-' + str(lss) + '.pth.tar'
            # torch.save(net.state_dict(), save_str)
            
            
#         print('Epoch loss: ' + str(tot_loss/tot_count))
        if save:
            im_format = 'png'
    #         im_format = 'eps'

            plt.figure(num=1)
            plt.savefig(net_name + '-01-loss.' + im_format)

            plt.figure(num=2)
            plt.savefig(net_name + '-02-accuracy.' + im_format)

            plt.figure(num=3)
            plt.savefig(net_name + '-03-accuracy-per-class.' + im_format)

            plt.figure(num=4)
            plt.savefig(net_name + '-04-prec-rec-fmeas.' + im_format)
        
    out = {'train_loss': epoch_train_loss[-1],
           'train_accuracy': epoch_train_accuracy[-1],
           'train_nochange_accuracy': epoch_train_nochange_accuracy[-1],
           'train_change_accuracy': epoch_train_change_accuracy[-1],
           'validation_loss': epoch_validation_loss[-1],
           'validation_accuracy': epoch_validation_accuracy[-1],
           'validation_nochange_accuracy': epoch_validation_nochange_accuracy[-1],
           'validation_change_accuracy': epoch_validation_change_accuracy[-1]}
    
    print('pr_c, rec_c, f_meas, pr_nc, rec_nc')
    print(pr_rec)
    
    return out







def validationFromTrain(dset):
    net.eval()
    tot_loss = 0
    tot_count = 0
    tot_accurate = 0
    
    n = 2
    class_correct = list(0. for i in range(n))
    class_total = list(0. for i in range(n))
    class_accuracy = list(0. for i in range(n))
    
    tp = 0
    tn = 0
    fp = 0
    fn = 0
    
    for img_index in dset.names:
        I1_full, I2_full, cm_full = dset.get_img(img_index)
        
        s = cm_full.shape
        

        steps0 = np.arange(0,s[0],ceil(s[0]/N))
        steps1 = np.arange(0,s[1],ceil(s[1]/N))
        for ii in range(N):
            for jj in range(N):
                xmin = steps0[ii]
                if ii == N-1:
                    xmax = s[0]
                else:
                    xmax = steps0[ii+1]
                ymin = jj
                if jj == N-1:
                    ymax = s[1]
                else:
                    ymax = steps1[jj+1]
                I1 = I1_full[:, xmin:xmax, ymin:ymax]
                I2 = I2_full[:, xmin:xmax, ymin:ymax]
                cm = cm_full[xmin:xmax, ymin:ymax]

                I1 = Variable(torch.unsqueeze(I1, 0).float()).cuda()
                I2 = Variable(torch.unsqueeze(I2, 0).float()).cuda()
                cm = Variable(torch.unsqueeze(torch.from_numpy(1.0*cm),0).float()).cuda()


                output = net(I1, I2)
                loss = criterion(output, cm.long())
        #         print(loss)
                tot_loss += loss.data * np.prod(cm.size())
                tot_count += np.prod(cm.size())

                _, predicted = torch.max(output.data, 1)

                c = (predicted.int() == cm.data.int())
                for i in range(c.size(1)):
                    for j in range(c.size(2)):
                        l = int(cm.data[0, i, j])
                        class_correct[l] += c[0, i, j]
                        class_total[l] += 1
                        
                pr = (predicted.int() > 0).cpu().numpy()
                gt = (cm.data.int() > 0).cpu().numpy()
                
                tp += np.logical_and(pr, gt).sum()
                tn += np.logical_and(np.logical_not(pr), np.logical_not(gt)).sum()
                fp += np.logical_and(pr, np.logical_not(gt)).sum()
                fn += np.logical_and(np.logical_not(pr), gt).sum()
        
    net_loss = tot_loss/tot_count
    net_accuracy = 100 * (tp + tn)/tot_count
    
    for i in range(n):
        class_accuracy[i] = 100 * class_correct[i] / max(class_total[i],0.00001)

    prec = tp / (tp + fp)
    rec = tp / (tp + fn)
    f_meas = 2 * prec * rec / (prec + rec)
    prec_nc = tn / (tn + fn)
    rec_nc = tn / (tn + fp)
    
    pr_rec = [prec, rec, f_meas, prec_nc, rec_nc]
        
    return net_loss, net_accuracy, class_accuracy, pr_rec





def save_test_results(dset):
    for name in tqdm(dset.names):
        with warnings.catch_warnings():
            I1, I2, cm = dset.get_img(name)
            I1 = Variable(torch.unsqueeze(I1, 0).float()).cuda()
            I2 = Variable(torch.unsqueeze(I2, 0).float()).cuda()
            out = net(I1, I2)
            _, predicted = torch.max(out.data, 1)
            I = np.stack((255*cm,255*np.squeeze(predicted.cpu().numpy()),255*cm),2)
            io.imsave(f'{net_name}-{name}.png',I)
            




def test(dset):
    net.eval()
    tot_loss = 0
    tot_count = 0
    tot_accurate = 0
    
    n = 2
    class_correct = list(0. for i in range(n))
    class_total = list(0. for i in range(n))
    class_accuracy = list(0. for i in range(n))
    
    tp = 0
    tn = 0
    fp = 0
    fn = 0
    
    for img_index in tqdm(dset.names):
        I1_full, I2_full, cm_full = dset.get_img(img_index)
        
        s = cm_full.shape
        
        for ii in range(ceil(s[0]/L)):
            for jj in range(ceil(s[1]/L)):
                xmin = L*ii
                xmax = min(L*(ii+1),s[1])
                ymin = L*jj
                ymax = min(L*(jj+1),s[1])
                I1 = I1_full[:, xmin:xmax, ymin:ymax]
                I2 = I2_full[:, xmin:xmax, ymin:ymax]
                cm = cm_full[xmin:xmax, ymin:ymax]

                I1 = Variable(torch.unsqueeze(I1, 0).float()).cuda()
                I2 = Variable(torch.unsqueeze(I2, 0).float()).cuda()
                cm = Variable(torch.unsqueeze(torch.from_numpy(1.0*cm),0).float()).cuda()

                output = net(I1, I2)
                    
                loss = criterion(output, cm.long())
                tot_loss += loss.data * np.prod(cm.size())
                tot_count += np.prod(cm.size())

                _, predicted = torch.max(output.data, 1)

                c = (predicted.int() == cm.data.int())
                for i in range(c.size(1)):
                    for j in range(c.size(2)):
                        l = int(cm.data[0, i, j])
                        class_correct[l] += c[0, i, j]
                        class_total[l] += 1
                        
                pr = (predicted.int() > 0).cpu().numpy()
                gt = (cm.data.int() > 0).cpu().numpy()
                
                tp += np.logical_and(pr, gt).sum()
                tn += np.logical_and(np.logical_not(pr), np.logical_not(gt)).sum()
                fp += np.logical_and(pr, np.logical_not(gt)).sum()
                fn += np.logical_and(np.logical_not(pr), gt).sum()
        
    tp = tp.astype('float64')
    tn = tn.astype('float64')
    fp = fp.astype('float64')
    fn = fn.astype('float64')
    
    net_loss = tot_loss/tot_count        
    net_loss = float(net_loss.cpu().numpy())
    
    net_accuracy = 100 * (tp + tn)/tot_count
    
    for i in range(n):
        class_accuracy[i] = 100 * class_correct[i] / max(class_total[i],0.00001)
        class_accuracy[i] =  float(class_accuracy[i].cpu().numpy())

    prec = tp / (tp + fp)
    rec = tp / (tp + fn)
    dice = 2 * prec * rec / (prec + rec)
    prec_nc = tn / (tn + fn)
    rec_nc = tn / (tn + fp)
    
    sensitivity=tp/(tp+fn)
    specificity=tn/(tn+fp)
    
    pr_rec = [prec, rec, dice, prec_nc, rec_nc]
    
    k = kappa(tp, tn, fp, fn)
    
    return {'net_loss': net_loss, 
            'net_accuracy': net_accuracy, 
            'class_accuracy': class_accuracy, 
            'precision': prec, 
            'recall': rec, 
            'dice': dice, 
            'sensitivity': sensitivity,
            'specificity': specificity,
            'tp': tp,
            'tn': tn,
            'fp': fp,
            'fn': fn,
            'kappa': k}




def testResultConfidenceAnalysis(dset):
    for name in tqdm(dset.names):
        with warnings.catch_warnings():
            I1, I2, cm = dset.get_img(name)
            I1 = Variable(torch.unsqueeze(I1, 0).float()).cuda()
            I2 = Variable(torch.unsqueeze(I2, 0).float()).cuda()
            cm = Variable(torch.unsqueeze(torch.from_numpy(1.0*cm),0).float()).cuda()
            softmaxOut, logitOut = netConfidenceAnalysis(I1, I2) ##for a (433,469) size image logit shape is (1,2,433,469)
            logitOutSqueezed = torch.squeeze(logitOut) ##Removing the extra 0-th dimension
            maxLogitPerPixel,_ = torch.max(logitOutSqueezed,0)
            confidenceScore = torch.mean(maxLogitPerPixel)
            
            # maxLogitPerPixelFlattened = torch.flatten(maxLogitPerPixel)
            # maxLogitPerPixelFlattenedTopK = torch.topk(maxLogitPerPixelFlattened, (torch.mul(maxLogitPerPixelFlattened.shape.numel(),0.95)).int())[0]
            # confidenceScore = torch.mean(maxLogitPerPixelFlattenedTopK)
            print('For City '+name+' trimmed logit based confidence score is: '+str(confidenceScore))
            
            
                       
            softmaxOutSqueezed = torch.squeeze(softmaxOut) ##Removing the extra 0-th dimension
            maxSoftmaxPerPixel,_ = torch.max(softmaxOutSqueezed,0)
            maxSoftmaxPerPixelForHistogram = maxSoftmaxPerPixel.cpu().detach().numpy()
            plt.hist(maxSoftmaxPerPixelForHistogram,bins=25)
            plt.savefig('./histogramPlots/'+name+'HistogramPlot.png')
            confidenceScore = torch.mean(maxSoftmaxPerPixel)
            print('For City '+name+'softmax based confidence score is: '+str(confidenceScore))
            
            
            output = net(I1, I2)
            _, predicted = torch.max(output.data, 1)
                    
            pr = (predicted.int() > 0).cpu().numpy()
            gt = (cm.data.int() > 0).cpu().numpy()
            tp = np.logical_and(pr, gt).sum()
            tn = np.logical_and(np.logical_not(pr), np.logical_not(gt)).sum()
            fp = np.logical_and(pr, np.logical_not(gt)).sum()
            fn = np.logical_and(np.logical_not(pr), gt).sum()
            
            tp = tp.astype('float64')
            tn = tn.astype('float64')
            fp = fp.astype('float64')
            fn = fn.astype('float64')
            sensitivityThisCity=tp/(tp+fn)
            specificityThisCity = tn/(tn+fp)
            kThisCity = kappa(tp, tn, fp, fn)
            print('For City '+name+'sensitivity is: '+str(sensitivityThisCity))
            print('For City '+name+'specificity is: '+str(specificityThisCity))
            print('For City '+name+'kappa is: '+str(kThisCity))
            
            print('\n\n\n')
            
            
           


if __name__ == "__main__":

    if DATA_AUG:
        data_transform = tr.Compose([RandomFlip(), RandomRot()])
    else:
        data_transform = None
    
    
            
    
    train_dataset = ChangeDetectionDataset(PATH_TO_DATASET, 'train', stride = TRAIN_STRIDE, transform=data_transform)
    weights = torch.FloatTensor(train_dataset.weights).cuda()
    print(weights)
    train_loader = DataLoader(train_dataset, batch_size = BATCH_SIZE, shuffle = True, num_workers = 0)
    
    test_dataset = ChangeDetectionDataset(PATH_TO_DATASET, 'test', stride = TRAIN_STRIDE)
    test_loader = DataLoader(test_dataset, batch_size = BATCH_SIZE, shuffle = True, num_workers = 0)
    
    validation_dataset = ChangeDetectionDataset(PATH_TO_DATASET, 'validation', stride = TRAIN_STRIDE)
    validation_loader = DataLoader(validation_dataset, batch_size = BATCH_SIZE, shuffle = True, num_workers = 0)
    
    
    # 0-RGB | 1-RGBIr | 2-All bands s.t. resulution <= 20m | 3-All bands
    
    if TYPE == 0:
    #     net, net_name = Unet(2*3, 2), 'FC-EF'
    #     net, net_name = SiamUnet_conc(3, 2), 'FC-Siam-conc'
    #     net, net_name = SiamUnet_diff(3, 2), 'FC-Siam-diff'
        net, net_name = FresUNet(2*3, 2), 'FresUNet'
    elif TYPE == 1:
    #     net, net_name = Unet(2*4, 2), 'FC-EF'
    #     net, net_name = SiamUnet_conc(4, 2), 'FC-Siam-conc'
    #     net, net_name = SiamUnet_diff(4, 2), 'FC-Siam-diff'
        net, net_name = FresUNet(2*4, 2), 'FresUNet'
    elif TYPE == 2:
    #     net, net_name = Unet(2*10, 2), 'FC-EF'
    #     net, net_name = SiamUnet_conc(10, 2), 'FC-Siam-conc'
    #     net, net_name = SiamUnet_diff(10, 2), 'FC-Siam-diff'
        net, net_name = FresUNet(2*10, 2), 'FresUNet'
    elif TYPE == 3:
    #     net, net_name = Unet(2*13, 2), 'FC-EF'
    #     net, net_name = SiamUnet_conc(13, 2), 'FC-Siam-conc'
    #     net, net_name = SiamUnet_diff(13, 2), 'FC-Siam-diff'
        net, net_name = FresUNet(2*13, 2), 'FresUNet'
    
    
    net.cuda()
    
    criterion = nn.NLLLoss(weight=weights) # to be used with logsoftmax output
    
    print('Number of trainable parameters:', count_parameters(net))
    
    L = 1024
    N = 2
    
    
    
    if LOAD_TRAINED:
        net.load_state_dict(torch.load('net_final.pth.tar'))
        print('LOAD OK')
    else:
        t_start = time.time()
        out_dic = train(N_EPOCHS, False)
        t_end = time.time()
        print(out_dic)
        print('Elapsed time:')
        print(t_end - t_start)
        
    if not LOAD_TRAINED:
        torch.save(net.state_dict(), 'net_final.pth.tar')
        print('SAVE OK')
    
    
    t_start = time.time()
    ### save_test_results(train_dataset)
    
    #save_test_results(test_dataset)
    t_end = time.time()
    L = 1024
    # results = test(test_dataset)
    # print('Elapsed time: {}'.format(t_end - t_start))   
    # pprint(results)
    
    if LOAD_TRAINED:
        netConfidenceAnalysis, net_nameConfidenceAnalysis = FresUNetConfidenceAnalysis(2*4, 2), 'FresUNet'
        netConfidenceAnalysis.cuda()
        netConfidenceAnalysis.load_state_dict(torch.load('net_final.pth.tar'))
        testResultConfidenceAnalysis(test_dataset)
    