# Change Detection in Hyperdimensional Images using Untrained Models

Codes are available here
https://github.com/sudipansaha/hyperdimensionalCD

Paper is available here: https://ieeexplore.ieee.org/abstract/document/9582825
