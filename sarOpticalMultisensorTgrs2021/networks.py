# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import os
import torch
import torch.nn as nn
from torch.nn import init
from torch.optim import lr_scheduler
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import scipy.io as sio


##setting manual seeds
manualSeed=40
torch.manual_seed(manualSeed)
torch.cuda.manual_seed_all(manualSeed)
np.random.seed(manualSeed)






# CNN model 5 channel
class Model5Channel(nn.Module):
    def __init__(self,numberOfImageChannels,nFeaturesIntermediateLayers,nFeaturesFinalLayer):
        super(Model5Channel, self).__init__()
        
        ##see this page: https://discuss.pytorch.org/t/extracting-and-using-features-from-a-pretrained-model/20723
        #pretrainedModel = vgg16(pretrained=True)
        #self.Vgg16features = nn.Sequential(
         #           *list(pretrainedModel.features.children())[:3])
        
        kernelSize=3
        paddingSize=int((kernelSize-1)/2)
        self.conv1 = nn.Conv2d(numberOfImageChannels, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize )
        self.bn1 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv1.weight=torch.nn.init.kaiming_uniform_(self.conv1.weight)
        self.conv1.bias.data.fill_(0.01)
        self.bn1.bias.data.fill_(0.001)
        
        
        self.conv2 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn2 = nn.BatchNorm2d(nFeaturesIntermediateLayers) 
        self.conv2.weight=torch.nn.init.kaiming_uniform_(self.conv2.weight)
        self.conv2.bias.data.fill_(0.01)
        self.bn2.bias.data.fill_(0.001)
        
        self.conv3 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn3 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv3.weight=torch.nn.init.kaiming_uniform_(self.conv3.weight)
        self.conv3.bias.data.fill_(0.01)
        self.bn3.bias.data.fill_(0.001)
        
        self.conv4 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn4 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv4.weight=torch.nn.init.kaiming_uniform_(self.conv4.weight)
        self.conv4.bias.data.fill_(0.01)
        self.bn4.bias.data.fill_(0.001)
        
        
             
        
         
        
        self.conv5 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesFinalLayer, kernel_size=1, stride=1, padding=0 )
        self.bn5 = nn.BatchNorm2d(nFeaturesFinalLayer)
        self.conv5.weight=torch.nn.init.kaiming_uniform_(self.conv5.weight)
        self.conv5.bias.data.fill_(0.01)
        self.bn5.bias.data.fill_(0.001)
        

    def forward(self, xModality1, xModality2):
        xModality1 = self.conv1(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn1(xModality1)
        xModality1 = self.conv2(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn2(xModality1)
        xModality1 = self.conv3(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn3(xModality1)
        xModality1 = self.conv4(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn4(xModality1)
        xModality1 = self.conv5(xModality1)
        xModality1 = self.bn5(xModality1)  ##Note there is no relu between last conv and bn
        
        xModality2 = self.conv1(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn1(xModality2)
        xModality2 = self.conv2(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn2(xModality2)
        xModality2 = self.conv3(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn3(xModality2)
        xModality2 = self.conv4(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn4(xModality2)
        xModality2 = self.conv5(xModality2)
        xModality2 = self.bn5(xModality2)  ##Note there is no relu between last conv and bn
        
     
        
        return xModality1,xModality2


















# CNN model 5 channel
class Model5ChannelInitialToMiddleLayersDifferent(nn.Module):
    def __init__(self,numberOfImageChannels,nFeaturesIntermediateLayers,nFeaturesFinalLayer):
        super(Model5ChannelInitialToMiddleLayersDifferent, self).__init__()
        
        ##see this page: https://discuss.pytorch.org/t/extracting-and-using-features-from-a-pretrained-model/20723
        #pretrainedModel = vgg16(pretrained=True)
        #self.Vgg16features = nn.Sequential(
         #           *list(pretrainedModel.features.children())[:3])
        
        kernelSize=3
        paddingSize=int((kernelSize-1)/2)
        self.conv1Modality1 = nn.Conv2d(numberOfImageChannels, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize )
        self.bn1Modality1 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv1Modality1.weight=torch.nn.init.kaiming_uniform_(self.conv1Modality1.weight)
        self.conv1Modality1.bias.data.fill_(0.01)
        #self.bn1.weight.data.fill_(1)
        self.bn1Modality1.bias.data.fill_(0.001)
        
        
        self.conv1Modality2 = nn.Conv2d(numberOfImageChannels, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize )
        self.bn1Modality2 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv1Modality2.weight=torch.nn.init.kaiming_uniform_(self.conv1Modality2.weight)
        self.conv1Modality2.bias.data.fill_(0.01)
        #self.bn1.weight.data.fill_(1)
        self.bn1Modality2.bias.data.fill_(0.001)
        
        
        self.conv2Modality1 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn2Modality1 = nn.BatchNorm2d(nFeaturesIntermediateLayers) 
        self.conv2Modality1.weight=torch.nn.init.kaiming_uniform_(self.conv2Modality1.weight)
        self.conv2Modality1.bias.data.fill_(0.01)
        #self.bn2Modality1.weight.data.fill_(1)
        self.bn2Modality1.bias.data.fill_(0.001)
        self.conv3Modality1 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn3Modality1 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv3Modality1.weight=torch.nn.init.kaiming_uniform_(self.conv3Modality1.weight)
        self.conv3Modality1.bias.data.fill_(0.01)
        #self.bn3Modality1.weight.data.fill_(1)
        self.bn3Modality1.bias.data.fill_(0.001)
        self.conv4Modality1 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn4Modality1 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv4Modality1.weight=torch.nn.init.kaiming_uniform_(self.conv4Modality1.weight)
        self.conv4Modality1.bias.data.fill_(0.01)
        #self.bn3Modality1.weight.data.fill_(1)
        self.bn4Modality1.bias.data.fill_(0.001)
        
        
        self.conv2Modality2 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn2Modality2 = nn.BatchNorm2d(nFeaturesIntermediateLayers) 
        self.conv2Modality2.weight=torch.nn.init.kaiming_uniform_(self.conv2Modality2.weight)
        self.conv2Modality2.bias.data.fill_(0.01)
        #self.bn2Modality1.weight.data.fill_(1)
        self.bn2Modality2.bias.data.fill_(0.001)
        self.conv3Modality2 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn3Modality2 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv3Modality2.weight=torch.nn.init.kaiming_uniform_(self.conv3Modality2.weight)
        self.conv3Modality2.bias.data.fill_(0.01)
        #self.bn3Modality1.weight.data.fill_(1)
        self.bn3Modality2.bias.data.fill_(0.001)
        self.conv4Modality2 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn4Modality2 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv4Modality2.weight=torch.nn.init.kaiming_uniform_(self.conv4Modality2.weight)
        self.conv4Modality2.bias.data.fill_(0.01)
        #self.bn3Modality1.weight.data.fill_(1)
        self.bn4Modality2.bias.data.fill_(0.001)
        
        
         
        
        self.conv5 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesFinalLayer, kernel_size=1, stride=1, padding=0 )
        self.bn5 = nn.BatchNorm2d(nFeaturesFinalLayer)
        self.conv5.weight=torch.nn.init.kaiming_uniform_(self.conv5.weight)
        self.conv5.bias.data.fill_(0.01)
        #self.bn4.weight.data.fill_(1)
        self.bn5.bias.data.fill_(0.001)
        

    def forward(self, xModality1, xModality2):
        #x = self.Vgg16features(x)
        xModality1 = self.conv1Modality1(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn1Modality1(xModality1)
        xModality1 = self.conv2Modality1(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn2Modality1(xModality1)
        xModality1 = self.conv3Modality1(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn3Modality1(xModality1)
        xModality1 = self.conv4Modality1(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn4Modality1(xModality1)
        xModality1 = self.conv5(xModality1)
        xModality1 = self.bn5(xModality1)  ##Note there is no relu between last conv and bn
        
        xModality2 = self.conv1Modality2(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn1Modality2(xModality2)
        xModality2 = self.conv2Modality2(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn2Modality2(xModality2)
        xModality2 = self.conv3Modality2(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn3Modality2(xModality2)
        xModality2 = self.conv4Modality2(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn4Modality2(xModality2)
        xModality2 = self.conv5(xModality2)
        xModality2 = self.bn5(xModality2)  ##Note there is no relu between last conv and bn
        
        return xModality1, xModality2



# CNN model 5 channel
class Model5ChannelInitialToMiddleLayersDifferentVaryingKernels(nn.Module):
    def __init__(self,numberOfImageChannels,nFeaturesIntermediateLayers,nFeaturesFinalLayer):
        super(Model5ChannelInitialToMiddleLayersDifferentVaryingKernels, self).__init__()
        
        ##see this page: https://discuss.pytorch.org/t/extracting-and-using-features-from-a-pretrained-model/20723
        #pretrainedModel = vgg16(pretrained=True)
        #self.Vgg16features = nn.Sequential(
         #           *list(pretrainedModel.features.children())[:3])
        
        kernelSize=3
        paddingSize=int((kernelSize-1)/2)
        self.conv1Modality1 = nn.Conv2d(numberOfImageChannels, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize )
        self.bn1Modality1 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv1Modality1.weight=torch.nn.init.kaiming_uniform_(self.conv1Modality1.weight)
        self.conv1Modality1.bias.data.fill_(0.01)
        #self.bn1.weight.data.fill_(1)
        self.bn1Modality1.bias.data.fill_(0.001)
        
        
        self.conv1Modality2 = nn.Conv2d(numberOfImageChannels, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize )
        self.bn1Modality2 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv1Modality2.weight=torch.nn.init.kaiming_uniform_(self.conv1Modality2.weight)
        self.conv1Modality2.bias.data.fill_(0.01)
        #self.bn1.weight.data.fill_(1)
        self.bn1Modality2.bias.data.fill_(0.001)
        
        
        self.conv2Modality1 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers*2, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn2Modality1 = nn.BatchNorm2d(nFeaturesIntermediateLayers*2) 
        self.conv2Modality1.weight=torch.nn.init.kaiming_uniform_(self.conv2Modality1.weight)
        self.conv2Modality1.bias.data.fill_(0.01)
        #self.bn2Modality1.weight.data.fill_(1)
        self.bn2Modality1.bias.data.fill_(0.001)
        self.conv3Modality1 = nn.Conv2d(nFeaturesIntermediateLayers*2, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn3Modality1 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv3Modality1.weight=torch.nn.init.kaiming_uniform_(self.conv3Modality1.weight)
        self.conv3Modality1.bias.data.fill_(0.01)
        #self.bn3Modality1.weight.data.fill_(1)
        self.bn3Modality1.bias.data.fill_(0.001)
        self.conv4Modality1 = nn.Conv2d(nFeaturesIntermediateLayers, int(nFeaturesIntermediateLayers/2), kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn4Modality1 = nn.BatchNorm2d(int(nFeaturesIntermediateLayers/2))
        self.conv4Modality1.weight=torch.nn.init.kaiming_uniform_(self.conv4Modality1.weight)
        self.conv4Modality1.bias.data.fill_(0.01)
        #self.bn3Modality1.weight.data.fill_(1)
        self.bn4Modality1.bias.data.fill_(0.001)
        
        
        self.conv2Modality2 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers*2, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn2Modality2 = nn.BatchNorm2d(nFeaturesIntermediateLayers*2) 
        self.conv2Modality2.weight=torch.nn.init.kaiming_uniform_(self.conv2Modality2.weight)
        self.conv2Modality2.bias.data.fill_(0.01)
        #self.bn2Modality1.weight.data.fill_(1)
        self.bn2Modality2.bias.data.fill_(0.001)
        self.conv3Modality2 = nn.Conv2d(nFeaturesIntermediateLayers*2, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn3Modality2 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv3Modality2.weight=torch.nn.init.kaiming_uniform_(self.conv3Modality2.weight)
        self.conv3Modality2.bias.data.fill_(0.01)
        #self.bn3Modality1.weight.data.fill_(1)
        self.bn3Modality2.bias.data.fill_(0.001)
        self.conv4Modality2 = nn.Conv2d(nFeaturesIntermediateLayers, int(nFeaturesIntermediateLayers/2), kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn4Modality2 = nn.BatchNorm2d(int(nFeaturesIntermediateLayers/2))
        self.conv4Modality2.weight=torch.nn.init.kaiming_uniform_(self.conv4Modality2.weight)
        self.conv4Modality2.bias.data.fill_(0.01)
        #self.bn3Modality1.weight.data.fill_(1)
        self.bn4Modality2.bias.data.fill_(0.001)
        
        
         
        
        self.conv5 = nn.Conv2d(int(nFeaturesIntermediateLayers/2), nFeaturesFinalLayer, kernel_size=1, stride=1, padding=0 )
        self.bn5 = nn.BatchNorm2d(nFeaturesFinalLayer)
        self.conv5.weight=torch.nn.init.kaiming_uniform_(self.conv5.weight)
        self.conv5.bias.data.fill_(0.01)
        #self.bn4.weight.data.fill_(1)
        self.bn5.bias.data.fill_(0.001)
        

    def forward(self, xModality1, xModality2):
        #x = self.Vgg16features(x)
        xModality1 = self.conv1Modality1(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn1Modality1(xModality1)
        xModality1 = self.conv2Modality1(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn2Modality1(xModality1)
        xModality1 = self.conv3Modality1(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn3Modality1(xModality1)
        xModality1 = self.conv4Modality1(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn4Modality1(xModality1)
        xModality1 = self.conv5(xModality1)
        xModality1 = self.bn5(xModality1)  ##Note there is no relu between last conv and bn
        
        xModality2 = self.conv1Modality2(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn1Modality2(xModality2)
        xModality2 = self.conv2Modality2(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn2Modality2(xModality2)
        xModality2 = self.conv3Modality2(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn3Modality2(xModality2)
        xModality2 = self.conv4Modality2(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn4Modality2(xModality2)
        xModality2 = self.conv5(xModality2)
        xModality2 = self.bn5(xModality2)  ##Note there is no relu between last conv and bn
        
        return xModality1, xModality2








# CNN model 4 channel
class Model4ChannelInitialToMiddleLayersDifferent(nn.Module):
    def __init__(self,numberOfImageChannels,nFeaturesIntermediateLayers,nFeaturesFinalLayer):
        super(Model4ChannelInitialToMiddleLayersDifferent, self).__init__()
        
        ##see this page: https://discuss.pytorch.org/t/extracting-and-using-features-from-a-pretrained-model/20723
        #pretrainedModel = vgg16(pretrained=True)
        #self.Vgg16features = nn.Sequential(
         #           *list(pretrainedModel.features.children())[:3])
        
        kernelSize=3
        paddingSize=int((kernelSize-1)/2)
        self.conv1Modality1 = nn.Conv2d(numberOfImageChannels, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize )
        self.bn1Modality1 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv1Modality1.weight=torch.nn.init.kaiming_uniform_(self.conv1Modality1.weight)
        self.conv1Modality1.bias.data.fill_(0.01)
        #self.bn1.weight.data.fill_(1)
        self.bn1Modality1.bias.data.fill_(0.001)
        
        
        self.conv1Modality2 = nn.Conv2d(numberOfImageChannels, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize )
        self.bn1Modality2 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv1Modality2.weight=torch.nn.init.kaiming_uniform_(self.conv1Modality2.weight)
        self.conv1Modality2.bias.data.fill_(0.01)
        #self.bn1.weight.data.fill_(1)
        self.bn1Modality2.bias.data.fill_(0.001)
        
        
        self.conv2Modality1 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn2Modality1 = nn.BatchNorm2d(nFeaturesIntermediateLayers) 
        self.conv2Modality1.weight=torch.nn.init.kaiming_uniform_(self.conv2Modality1.weight)
        self.conv2Modality1.bias.data.fill_(0.01)
        #self.bn2Modality1.weight.data.fill_(1)
        self.bn2Modality1.bias.data.fill_(0.001)
        self.conv3Modality1 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn3Modality1 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv3Modality1.weight=torch.nn.init.kaiming_uniform_(self.conv3Modality1.weight)
        self.conv3Modality1.bias.data.fill_(0.01)
        #self.bn3Modality1.weight.data.fill_(1)
        self.bn3Modality1.bias.data.fill_(0.001)
        
        
        self.conv2Modality2 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn2Modality2 = nn.BatchNorm2d(nFeaturesIntermediateLayers) 
        self.conv2Modality2.weight=torch.nn.init.kaiming_uniform_(self.conv2Modality2.weight)
        self.conv2Modality2.bias.data.fill_(0.01)
        #self.bn2Modality1.weight.data.fill_(1)
        self.bn2Modality2.bias.data.fill_(0.001)
        self.conv3Modality2 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn3Modality2 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv3Modality2.weight=torch.nn.init.kaiming_uniform_(self.conv3Modality2.weight)
        self.conv3Modality2.bias.data.fill_(0.01)
        #self.bn3Modality1.weight.data.fill_(1)
        self.bn3Modality2.bias.data.fill_(0.001)
        
        
        
        self.conv4 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesFinalLayer, kernel_size=1, stride=1, padding=0 )
        self.bn4 = nn.BatchNorm2d(nFeaturesFinalLayer)
        self.conv4.weight=torch.nn.init.kaiming_uniform_(self.conv4.weight)
        self.conv4.bias.data.fill_(0.01)
        #self.bn4.weight.data.fill_(1)
        self.bn4.bias.data.fill_(0.001)
        

    def forward(self, xModality1, xModality2):
        #x = self.Vgg16features(x)
        xModality1 = self.conv1Modality1(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn1Modality1(xModality1)
        xModality1 = self.conv2Modality1(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn2Modality1(xModality1)
        xModality1 = self.conv3Modality1(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn3Modality1(xModality1)
        xModality1 = self.conv4(xModality1)
        xModality1 = self.bn4(xModality1)  ##Note there is no relu between last conv and bn
        
        xModality2 = self.conv1Modality2(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn1Modality2(xModality2)
        xModality2 = self.conv2Modality2(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn2Modality2(xModality2)
        xModality2 = self.conv3Modality2(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn3Modality2(xModality2)
        xModality2 = self.conv4(xModality2)
        xModality2 = self.bn4(xModality2)  ##Note there is no relu between last conv and bn
        
        return xModality1, xModality2



# CNN model 4 channel
class Model4ChannelMiddleLayersDifferent(nn.Module):
    def __init__(self,numberOfImageChannels,nFeaturesIntermediateLayers,nFeaturesFinalLayer):
        super(Model4ChannelMiddleLayersDifferent, self).__init__()
        
        ##see this page: https://discuss.pytorch.org/t/extracting-and-using-features-from-a-pretrained-model/20723
        #pretrainedModel = vgg16(pretrained=True)
        #self.Vgg16features = nn.Sequential(
         #           *list(pretrainedModel.features.children())[:3])
        
        kernelSize=3
        paddingSize=int((kernelSize-1)/2)
        self.conv1 = nn.Conv2d(numberOfImageChannels, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize )
        self.bn1 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv1.weight=torch.nn.init.kaiming_uniform_(self.conv1.weight)
        self.conv1.bias.data.fill_(0.01)
        #self.bn1.weight.data.fill_(1)
        self.bn1.bias.data.fill_(0.001)
        
        
        self.conv2Modality1 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn2Modality1 = nn.BatchNorm2d(nFeaturesIntermediateLayers) 
        self.conv2Modality1.weight=torch.nn.init.kaiming_uniform_(self.conv2Modality1.weight)
        self.conv2Modality1.bias.data.fill_(0.01)
        #self.bn2Modality1.weight.data.fill_(1)
        self.bn2Modality1.bias.data.fill_(0.001)
        self.conv3Modality1 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn3Modality1 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv3Modality1.weight=torch.nn.init.kaiming_uniform_(self.conv3Modality1.weight)
        self.conv3Modality1.bias.data.fill_(0.01)
        #self.bn3Modality1.weight.data.fill_(1)
        self.bn3Modality1.bias.data.fill_(0.001)
        
        
        self.conv2Modality2 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn2Modality2 = nn.BatchNorm2d(nFeaturesIntermediateLayers) 
        self.conv2Modality2.weight=torch.nn.init.kaiming_uniform_(self.conv2Modality2.weight)
        self.conv2Modality2.bias.data.fill_(0.01)
        #self.bn2Modality1.weight.data.fill_(1)
        self.bn2Modality2.bias.data.fill_(0.001)
        self.conv3Modality2 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn3Modality2 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv3Modality2.weight=torch.nn.init.kaiming_uniform_(self.conv3Modality2.weight)
        self.conv3Modality2.bias.data.fill_(0.01)
        #self.bn3Modality1.weight.data.fill_(1)
        self.bn3Modality2.bias.data.fill_(0.001)
        
        
        
        self.conv4 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesFinalLayer, kernel_size=1, stride=1, padding=0 )
        self.bn4 = nn.BatchNorm2d(nFeaturesFinalLayer)
        self.conv4.weight=torch.nn.init.kaiming_uniform_(self.conv4.weight)
        self.conv4.bias.data.fill_(0.01)
        #self.bn4.weight.data.fill_(1)
        self.bn4.bias.data.fill_(0.001)
        

    def forward(self, xModality1, xModality2):
        #x = self.Vgg16features(x)
        xModality1 = self.conv1(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn1(xModality1)
        xModality1 = self.conv2Modality1(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn2Modality1(xModality1)
        xModality1 = self.conv3Modality1(xModality1)
        xModality1 = F.relu( xModality1 )
        xModality1 = self.bn3Modality1(xModality1)
        xModality1 = self.conv4(xModality1)
        xModality1 = self.bn4(xModality1)  ##Note there is no relu between last conv and bn
        
        xModality2 = self.conv1(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn1(xModality2)
        xModality2 = self.conv2Modality2(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn2Modality2(xModality2)
        xModality2 = self.conv3Modality2(xModality2)
        xModality2 = F.relu( xModality2 )
        xModality2 = self.bn3Modality2(xModality2)
        xModality2 = self.conv4(xModality2)
        xModality2 = self.bn4(xModality2)  ##Note there is no relu between last conv and bn
        
        return xModality1, xModality2
        








    
    
# CNN model 4 channel
class Model4Channel(nn.Module):
    def __init__(self,numberOfImageChannels,nFeaturesIntermediateLayers,nFeaturesFinalLayer):
        super(Model4Channel, self).__init__()
        
        ##see this page: https://discuss.pytorch.org/t/extracting-and-using-features-from-a-pretrained-model/20723
        #pretrainedModel = vgg16(pretrained=True)
        #self.Vgg16features = nn.Sequential(
         #           *list(pretrainedModel.features.children())[:3])
        
        kernelSize=3
        paddingSize=int((kernelSize-1)/2)
        self.conv1 = nn.Conv2d(numberOfImageChannels, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize )
        self.bn1 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv1.weight=torch.nn.init.kaiming_uniform_(self.conv1.weight)
        self.conv1.bias.data.fill_(0.01)
        self.bn1.weight.data.fill_(1)
        self.bn1.bias.data.fill_(0.001)
        self.conv2 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn2 = nn.BatchNorm2d(nFeaturesIntermediateLayers) 
        self.conv2.weight=torch.nn.init.kaiming_uniform_(self.conv2.weight)
        self.conv2.bias.data.fill_(0.01)
        self.bn2.weight.data.fill_(1)
        self.bn2.bias.data.fill_(0.001)
        self.conv3 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn3 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv3.weight=torch.nn.init.kaiming_uniform_(self.conv3.weight)
        self.conv3.bias.data.fill_(0.01)
        self.bn3.weight.data.fill_(1)
        self.bn3.bias.data.fill_(0.001)
        self.conv4 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesFinalLayer, kernel_size=1, stride=1, padding=0 )
        self.bn4 = nn.BatchNorm2d(nFeaturesFinalLayer)
        self.conv4.weight=torch.nn.init.kaiming_uniform_(self.conv4.weight)
        self.conv4.bias.data.fill_(0.01)
        self.bn4.weight.data.fill_(1)
        self.bn4.bias.data.fill_(0.001)
        

    def forward(self, x):
        #x = self.Vgg16features(x)
        x = self.conv1(x)
        x = F.relu( x )
        x = self.bn1(x)
        x = self.conv2(x)
        x = F.relu( x )
        x = self.bn2(x)
        x = self.conv3(x)
        x = F.relu( x )
        x = self.bn3(x)
        x = self.conv4(x)
        x = self.bn4(x)  ##Note there is no relu between last conv and bn
        
        return x
        
        
class Model3Channel(nn.Module):
    def __init__(self,numberOfImageChannels,nFeaturesIntermediateLayers,nFeaturesFinalLayer):
        super(Model3Channel, self).__init__()
        
        ##see this page: https://discuss.pytorch.org/t/extracting-and-using-features-from-a-pretrained-model/20723
        #pretrainedModel = vgg16(pretrained=True)
        #self.Vgg16features = nn.Sequential(
         #           *list(pretrainedModel.features.children())[:3])
        
        kernelSize=3
        paddingSize=int((kernelSize-1)/2)
        self.conv1 = nn.Conv2d(numberOfImageChannels, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize )
        self.bn1 = nn.BatchNorm2d(nFeaturesIntermediateLayers)
        self.conv1.weight=torch.nn.init.kaiming_uniform_(self.conv1.weight)
        self.conv1.bias.data.fill_(0.01)
        self.bn1.weight.data.fill_(1)
        self.bn1.bias.data.fill_(0.001)
        self.conv2 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.bn2 = nn.BatchNorm2d(nFeaturesIntermediateLayers) 
        self.conv2.weight=torch.nn.init.kaiming_uniform_(self.conv2.weight)
        self.conv2.bias.data.fill_(0.01)
        self.bn2.weight.data.fill_(1)
        self.bn2.bias.data.fill_(0.001)
        self.conv3 = nn.Conv2d(nFeaturesIntermediateLayers, nFeaturesFinalLayer, kernel_size=1, stride=1, padding=paddingSize )
        self.bn3 = nn.BatchNorm2d(nFeaturesFinalLayer)
        self.conv3.weight=torch.nn.init.kaiming_uniform_(self.conv3.weight)
        self.conv3.bias.data.fill_(0.01)
        self.bn3.weight.data.fill_(1)
        self.bn3.bias.data.fill_(0.001)
        
                

    def forward(self, x):
        #x = self.Vgg16features(x)
        x = self.conv1(x)
        x = F.relu( x )
        x = self.bn1(x)
        x = self.conv2(x)
        x = F.relu( x )
        x = self.bn2(x)
        x = self.conv3(x)
        x = self.bn3(x)  ##Note there is no relu between last conv and bn
        
        return x
    
    


# CNN model 4 channel
class Model4ChannelDownsampleUpsample(nn.Module):
    def __init__(self,numberOfImageChannels,nFeaturesIntermediateLayers,nFeaturesFinalLayer):
        super(Model4ChannelDownsampleUpsample, self).__init__()
        
        ##see this page: https://discuss.pytorch.org/t/extracting-and-using-features-from-a-pretrained-model/20723
        #pretrainedModel = vgg16(pretrained=True)
        #self.Vgg16features = nn.Sequential(
         #           *list(pretrainedModel.features.children())[:3])
        
        kernelSize=3
        paddingSize=int((kernelSize-1)/2)
        self.conv1 = nn.Conv2d(numberOfImageChannels, int(nFeaturesIntermediateLayers/2), kernel_size=kernelSize, stride=1, padding=paddingSize )
        self.pooling1=nn.MaxPool2d((2, 2))
        self.bn1 = nn.BatchNorm2d(int(nFeaturesIntermediateLayers/2))
        self.conv1.weight=torch.nn.init.kaiming_uniform_(self.conv1.weight)
        self.conv1.bias.data.fill_(0.01)
        #self.bn1.weight.data.fill_(1)
        self.bn1.bias.data.fill_(0.001)
        
        
        self.conv2 = nn.Conv2d(int(nFeaturesIntermediateLayers/2), nFeaturesIntermediateLayers, kernel_size=kernelSize, stride=1, padding=paddingSize ) 
        self.pooling2=nn.MaxPool2d((2, 2))
        self.bn2 = nn.BatchNorm2d(nFeaturesIntermediateLayers) 
        self.conv2.weight=torch.nn.init.kaiming_uniform_(self.conv2.weight)
        self.conv2.bias.data.fill_(0.01)
        #self.bn2.weight.data.fill_(1)
        self.bn2.bias.data.fill_(0.001)
        
        self.conv3 = nn.ConvTranspose2d(nFeaturesIntermediateLayers, int(nFeaturesIntermediateLayers/2), kernel_size=kernelSize, stride=2, padding = paddingSize, output_padding=1 ) 
        self.bn3 = nn.BatchNorm2d(int(nFeaturesIntermediateLayers/2))
        self.conv3.weight=torch.nn.init.kaiming_uniform_(self.conv3.weight)
        self.conv3.bias.data.fill_(0.01)
        #self.bn3.weight.data.fill_(1)
        self.bn3.bias.data.fill_(0.001)
        
        self.conv4ForImage1 = nn.ConvTranspose2d(int(nFeaturesIntermediateLayers/2), nFeaturesFinalLayer, kernel_size=kernelSize, stride=2, padding = paddingSize, output_padding=1 )
        self.bn4ForImage1 = nn.BatchNorm2d(nFeaturesFinalLayer)
        self.conv4ForImage1.weight=torch.nn.init.kaiming_uniform_(self.conv4ForImage1.weight)
        self.conv4ForImage1.bias.data.fill_(0.01)
        self.bn4ForImage1.weight.data.fill_(1)
        self.bn4ForImage1.bias.data.fill_(0.001)
        
        self.conv4ForImage2 = nn.ConvTranspose2d(int(nFeaturesIntermediateLayers/2), nFeaturesFinalLayer, kernel_size=kernelSize, stride=2, padding = paddingSize, output_padding=1 ) 
        self.bn4ForImage2 = nn.BatchNorm2d(nFeaturesFinalLayer)
        self.conv4ForImage2.weight=torch.nn.init.kaiming_uniform_(self.conv4ForImage2.weight)
        self.conv4ForImage2.bias.data.fill_(0.01)
        self.bn4ForImage2.weight.data.fill_(1)
        self.bn4ForImage2.bias.data.fill_(0.001)
        
    def base(self, x): 
        x = self.conv1(x)
        x = self.pooling1(x)
        x = F.relu( x )
        x = self.bn1(x)
        
        x = self.conv2(x)
        x = self.pooling2(x)
        x = F.relu( x )
        x = self.bn2(x)
        
        
        x = self.conv3(x)
        x = F.relu( x )
        x = self.bn3(x)
        return x
        
   
    
    def forward(self, x1,x2):
        #x = self.Vgg16features(x)
        x1 = self.base(x1)
        x2 = self.base(x2)
        
        xImage1 = self.conv4ForImage1(x1)
        xImage1 = self.bn4ForImage1(xImage1)  ##Note there is no relu between last conv and bn
        
        xImage2 = self.conv4ForImage2(x2)
        xImage2 = self.bn4ForImage2(xImage2)  ##Note there is no relu between last conv and bn
        
        
        return xImage1,xImage2








