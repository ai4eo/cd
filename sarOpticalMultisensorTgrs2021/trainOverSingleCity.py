# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import os
import glob
import torch
import torch.nn as nn
from torch.nn import init
from torch.optim import lr_scheduler
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
plt.rc('text', usetex=True)
from torch.utils.data import Dataset, DataLoader
import torchvision.transforms as transforms



from skimage.transform import resize
from skimage import filters
from skimage import morphology
from skimage.filters import rank
#import cv2 as cv

import random
import scipy.stats as sistats
import scipy
from sklearn.metrics import confusion_matrix
from sklearn.mixture import GaussianMixture
from sklearn.cluster import KMeans
import tifffile 
import matplotlib.image as mpimg

from tqdm import tqdm as tqdm
from math import floor, ceil, sqrt, exp
from skimage import io

from preprocessInput import preprocessSentinel2,preprocessSentinel1,getImageStatisticsForNormalizing


from skimage.transform import resize
from skimage import filters
from skimage.morphology import disk
from tifffile import imsave

from networks import Model5ChannelInitialToMiddleLayersDifferent
from osgeo import gdal
import time


nanVar=float('nan')

##Defining data paths
time1DirectoryPath = '../data/Onera_SR_2020_12_15/OSCDImages/'
time2DirectoryPath = '../data/Onera_S1/'
labelDirectoryPath = '../data/Onera_SR_2020_12_15/OSCDLabels/'

##Defining parameters
topPercentToSaturate=1
whichOrderForPrechangePostchange = 'S2AndS1'
trainingPatchSize=64 ##Patch size used for training self-sup learning
saveModelPathPrefix = './trainedModels/'+'model'+whichOrderForPrechangePostchange
numberOfImageChannels = 3
positiveNegativeTemperature = 1

### Paramsters related to the CNN model
modelInputMean=0
trainingBatchSize = 8
nFeaturesIntermediateLayers = 64  ##number of features in the intermediate layers
nFeaturesFinalLayer = 4 ##number of features of final classification layer
numTrainingEpochs = 5
maxIter = 50  ##number of maximum iterations over same batch
maxIterDropPerEpoch = 0
lr = 0.001 ##Learning rate 
nConv = 3  ##number of convolutional layers
useCuda = torch.cuda.is_available()   


##setting manual seeds
manualSeed=40
torch.manual_seed(manualSeed)
torch.cuda.manual_seed_all(manualSeed)
np.random.seed(manualSeed)



#training City Lists
trainingCityNameList=[]
# #
# trainingCityNameList.append('brasilia') ##433*469
# trainingCityNameList.append('chongqing') ##730*544
# trainingCityNameList.append('dubai')      ##774*634
trainingCityNameList.append('lasvegas')   ##824*716
# trainingCityNameList.append('milano')     ##545*558
# trainingCityNameList.append('montpellier') ##426*451
# trainingCityNameList.append('norcia')   ##241*385
# trainingCityNameList.append('rio')      ##353*426
# trainingCityNameList.append('saclay_w')  ##639*688
# trainingCityNameList.append('valencia')  ##458*476

### good size training cities: beihai, beirut, abudhabi, cupertino, mumbai, hongkong
# trainingCityNameList.append('cupertino')









class OSCDDatasetLoader(Dataset):
    """Change Detection dataset class, used for both training and test data."""

    def __init__(self, cityNames, whichOrderForPrechangePostchange, patchSize = 112, stride = None, transform=None):
        """
        Argumets:
            
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        
        # basics
        self.transform = transform
        self.patchSize = patchSize
        if not stride:
            self.stride = 1
        else:
            self.stride = stride
        

        self.cityNames = cityNames
        self.n_imgs = len(self.cityNames)
        
               
        
        # load images
        self.imgs_1 = {}
        self.imgs_2 = {}
        self.change_maps = {}
        self.n_patches_per_image = {}
        self.n_patches = 0
        self.patch_coords = []
        for cityName in tqdm(self.cityNames):
            time1Path = time1DirectoryPath+cityName
            time2Path = time2DirectoryPath+cityName
            labelPath = labelDirectoryPath+cityName
            
            # load and store each image
            I1, I2, cm = self.readSentinelImages(time1Path, time2Path, labelPath, whichOrderForPrechangePostchange)
            self.imgs_1[cityName] = self.reshape_for_torch(I1)
            self.imgs_2[cityName] = self.reshape_for_torch(I2)
            self.change_maps[cityName] = cm
            
                      
            # calculate the number of patches
            s = self.imgs_1[cityName].shape
            n1 = ceil((s[1] - self.patchSize + 1) / self.stride)
            n2 = ceil((s[2] - self.patchSize + 1) / self.stride)
            n_patches_i = n1 * n2
            self.n_patches_per_image[cityName] = n_patches_i
            self.n_patches += n_patches_i
            
            # generate path coordinates
            for i in range(n1):
                for j in range(n2):
                    # coordinates in (x1, x2, y1, y2)
                    current_patch_coords = (cityName, 
                                    [self.stride*i, self.stride*i + self.patchSize, self.stride*j, self.stride*j + self.patchSize],
                                    [self.stride*(i + 1), self.stride*(j + 1)])
                    self.patch_coords.append(current_patch_coords)
                    

        
        

    def get_img(self, im_name):
        return self.imgs_1[im_name], self.imgs_2[im_name], self.change_maps[im_name]

    def __len__(self):
        return self.n_patches

    def __getitem__(self, idx):
        current_patch_coords = self.patch_coords[idx]
        im_name = current_patch_coords[0]
        limits = current_patch_coords[1]
        
        I1 = self.imgs_1[im_name][:, limits[0]:limits[1], limits[2]:limits[3]]
        I2 = self.imgs_2[im_name][:, limits[0]:limits[1], limits[2]:limits[3]]
        
        label = self.change_maps[im_name][limits[0]:limits[1], limits[2]:limits[3]]
        label = torch.from_numpy(1*np.array(label)).float()
        
        sample = {'I1': I1, 'I2': I2, 'label': label}
        
        if self.transform:
            sample = self.transform(sample)

        return sample
    
    
    def readSentinelImages(self,time1Path, time2Path, labelPath, TYPE):
        """Read cropped Sentinel-2 image pair and change map."""

        if TYPE == 'S2AndS1':
            preChangeImageBand1 = tifffile.imread(time1Path+'/imgs_1_rect/'+'B04.tif')  
            preChangeImageBand1 = preprocessSentinel2(preChangeImageBand1,topPercentToSaturate)
            preChangeImageBand2 = tifffile.imread(time1Path+'/imgs_1_rect/'+'B03.tif')
            preChangeImageBand2 = preprocessSentinel2(preChangeImageBand2,topPercentToSaturate)
            preChangeImageBand3 = tifffile.imread(time1Path+'/imgs_1_rect/'+'B02.tif')        
            preChangeImageBand3 = preprocessSentinel2(preChangeImageBand3,topPercentToSaturate)
            I1 = np.stack((preChangeImageBand1,preChangeImageBand2,preChangeImageBand3),axis=2)
            
                       
                        
            S1PostChangeFilePath = glob.glob(os.path.join(time2Path+'/imgs_2/transformed/','*.tif'))
            S1PostChangeFilePath =  S1PostChangeFilePath[0]
            postChangeImageGdal = gdal.Open(S1PostChangeFilePath, gdal.GA_ReadOnly)
            I2 = preprocessSentinel1(postChangeImageGdal,topPercentToSaturate, I1.shape)
            
            # for bandIter in range(1, postChangeImageGdal.RasterCount +1):
            #     band = postChangeImageGdal.GetRasterBand(bandIter)
            #     inputImageThisChannel = band.ReadAsArray()
            #     print(inputImageThisChannel.shape)
           
       
            
           
            
            
            
           
        elif TYPE == 'S1AndS2':
            S1PreChangeFilePath = glob.glob(os.path.join(time1Path+'/imgs_1/transformed/','*.tif'))
            S1PreChangeFilePath =  S1PreChangeFilePath[0]
            preChangeImageGdal = gdal.Open(S1PreChangeFilePath, gdal.GA_ReadOnly)  
            I1 = preprocessSentinel1(preChangeImageGdal,topPercentToSaturate)
            
            postChangeImageBand1 = tifffile.imread(time2Path+'/imgs_2_rect/'+'B04.tif')
            postChangeImageBand1 = preprocessSentinel2(postChangeImageBand1,topPercentToSaturate)
            postChangeImageBand2 = tifffile.imread(time2Path+'/imgs_2_rect/'+'B03.tif')
            postChangeImageBand2 = preprocessSentinel2(postChangeImageBand2,topPercentToSaturate)
            postChangeImageBand3 = tifffile.imread(time2Path+'/imgs_2_rect/'+'B02.tif')
            postChangeImageBand3 = preprocessSentinel2(postChangeImageBand3,topPercentToSaturate)
            I2 = np.stack((postChangeImageBand1,postChangeImageBand2,postChangeImageBand3),axis=2)
            
        cm = io.imread(labelPath + '/cm/cm.png', as_gray=True) != 0
        
        # crop if necessary
        s1 = I1.shape
        s2 = I2.shape
        I2 = np.pad(I2,((0, s1[0] - s2[0]), (0, s1[1] - s2[1]), (0,0)),'edge')
        return I1, I2, cm
    
    
    def reshape_for_torch(self,I):
        """Transpose image for PyTorch coordinates."""
    #     out = np.swapaxes(I,1,2)
    #     out = np.swapaxes(out,0,1)
    #     out = out[np.newaxis,:]
        out = I.transpose((2, 0, 1))
        return torch.from_numpy(out)



class RandomFlip(object):
    """Flip randomly the images in a sample."""

#     def __init__(self):
#         return

    def __call__(self, sample):
        I1, I2, label = sample['I1'], sample['I2'], sample['label']
        
        if random.random() > 0.5:
            I1 =  I1.numpy()[:,:,::-1].copy()
            I1 = torch.from_numpy(I1)
            I2 =  I2.numpy()[:,:,::-1].copy()
            I2 = torch.from_numpy(I2)
            label =  label.numpy()[:,::-1].copy()
            label = torch.from_numpy(label)

        return {'I1': I1, 'I2': I2, 'label': label}



class RandomRot(object):
    """Rotate randomly the images in a sample."""

#     def __init__(self):
#         return

    def __call__(self, sample):
        I1, I2, label = sample['I1'], sample['I2'], sample['label']
        
        n = random.randint(0, 3)
        if n:
            I1 =  sample['I1'].numpy()
            I1 = np.rot90(I1, n, axes=(1, 2)).copy()
            I1 = torch.from_numpy(I1)
            I2 =  sample['I2'].numpy()
            I2 = np.rot90(I2, n, axes=(1, 2)).copy()
            I2 = torch.from_numpy(I2)
            label =  sample['label'].numpy()
            label = np.rot90(label, n, axes=(0, 1)).copy()
            label = torch.from_numpy(label)

        return {'I1': I1, 'I2': I2, 'label': label}

class Normalize(object):
    """substitute for transforms.normalize."""

#     def __init__(self):
#         return
    def __init__(self, mean1, std1, mean2, std2):
        super().__init__()
        self.mean1 = mean1
        self.std1 = std1
        self.mean2 = mean2
        self.std2 = std2
    
    def __call__(self, sample):        
        I1, I2, label = sample['I1'], sample['I2'], sample['label']
        norm1 = transforms.Normalize(self.mean1, self.std1)
        norm2 = transforms.Normalize(self.mean2, self.std2)
        
        I1 = norm1(I1)
        I2 = norm2(I2)

        return {'I1': I1, 'I2': I2, 'label': label}





for trainingCity in trainingCityNameList:   
    # model needs to be reinitialized for every city
    model = Model5ChannelInitialToMiddleLayersDifferent(numberOfImageChannels,nFeaturesIntermediateLayers,nFeaturesFinalLayer) 
    #model = Model5Channel(numberOfImageChannels,nFeaturesIntermediateLayers,nFeaturesFinalLayer) 
    #print(model)    
    if useCuda:
        model.cuda()     
    model.train()
    
    #model.Vgg16features.requires_grad = False
    lossFunction = torch.nn.CrossEntropyLoss()
    lossFunctionSecondary = torch.nn.L1Loss()
    optimizer = optim.SGD(model.parameters(), lr=lr, momentum=0.9) ##Adam or SGD
    #optimizer = optim.Adam(model.parameters(), lr=lr)  ##Adam or SGD
    
    
    currentTrainingCityNameList = []
    currentTrainingCityNameList.append(trainingCity)  ##making a new list with just one city in it
    
    mean1, std1, mean2, std2 = getImageStatisticsForNormalizing (trainingCity, time1DirectoryPath+trainingCity, time2DirectoryPath+trainingCity, whichOrderForPrechangePostchange,topPercentToSaturate)
    
    
    dataTransform = transforms.Compose([
                                       RandomFlip(),
                                       RandomRot(),
                                       #Normalize((0, 0, 0), (1, 1, 1))
                                       Normalize(mean1, std1, mean2, std2)
                                       ])
    trainingDataset = OSCDDatasetLoader(currentTrainingCityNameList, whichOrderForPrechangePostchange, trainingPatchSize, int(trainingPatchSize/2), dataTransform)
    
    
    
    trainLoader = torch.utils.data.DataLoader(dataset=trainingDataset,batch_size=trainingBatchSize,shuffle=True) ##shuffle does not matter here
    
    print(len(trainingDataset))
    print(len(trainLoader))
    x = 10/0
    
        
    lossPrimary1Array = torch.empty((1))
    lossPrimary2Array = torch.empty((1))
    lossSecondary1Array = torch.empty((1))
    lossSecondary2Array = torch.empty((1))
    
    torch.save(model,saveModelPathPrefix+'/0Epochs/'+trainingCity+'.pth')
    
    startTime = time.time()
    for epochIter in range(numTrainingEpochs):
        
        # if epochIter==5:
        #     optimizer = optim.SGD(model.parameters(), lr=lr/10, momentum=0.9) ##Adam or SGD
        for batchStep, batchData in enumerate(trainLoader):
            data1ForModelTraining = batchData['I1'].float().cuda()
            data2ForModelTraining = batchData['I2'].float().cuda()
            
           
            randomShufflingIndices = torch.randperm(data2ForModelTraining.shape[0])
            data2ForModelTrainingShuffled = data2ForModelTraining[randomShufflingIndices,:,:,:]
              
            thisBatchLoss = torch.empty((maxIter,1))
            for trainingInsideIter in range(maxIter):
                
                #label = torch.squeeze(Variable(batch['label'].cuda()))
                # forwarding
                optimizer.zero_grad()
                
                ##Computing primary loss from image 1
                ##input size is: batchSize*numChannel*Row*Col
                projection1, projection2 = model(data1ForModelTraining, data2ForModelTraining)
                
                #projection2 = model(data2ForModelTraining)
                
                _,projection2Shuffled = model(data1ForModelTraining,data2ForModelTrainingShuffled)
                
                
                
                _,prediction1 = torch.max(projection1,1)
                _,prediction2 = torch.max(projection2,1)
                _,prediction2Shuffled = torch.max(projection2Shuffled,1)
                
                lossPrimary1 = lossFunction(projection1, prediction1) 
                lossPrimary2 = lossFunction(projection2, prediction2)
                
                lossPrimary = (lossPrimary1+lossPrimary2)/2
                
                lossSecondary1 = lossFunctionSecondary(projection1,projection2)
                lossSecondary2 = -lossFunctionSecondary(projection1,projection2Shuffled)
                
                
                lossPrimary1Array = torch.cat((lossPrimary1Array,lossPrimary1.unsqueeze(0).cpu().detach()))
                lossPrimary2Array = torch.cat((lossPrimary2Array,lossPrimary2.unsqueeze(0).cpu().detach()))
                lossSecondary1Array = torch.cat((lossSecondary1Array,lossSecondary1.unsqueeze(0).cpu().detach()))
                lossSecondary2Array = torch.cat((lossSecondary2Array,lossSecondary2.unsqueeze(0).cpu().detach()))
                                              
               
                
                if epochIter==0:
                    lossPrimary.backward()
                    print(lossPrimary)
                else:
                
                    if trainingInsideIter%3==0:
                        lossPrimary1.backward()
                        print(lossPrimary)
                        
                    elif trainingInsideIter%3==1:
                        lossSecondary1.mean().backward()
                        print(lossSecondary1.mean())
                        
                        
                    else:
                        lossSecondary2.exp().mean().backward()
                        print(lossSecondary2.exp().mean())

                optimizer.step()
                
                
                            
                
                
                #thisBatchLoss[trainingInsideIter,0]=loss
            
           
            print ('Epoch: ',epochIter, '/', numTrainingEpochs, 'batch: ',batchStep)
        print('End of epoch', epochIter)
        
        maxIter = np.max((maxIter-maxIterDropPerEpoch,10))  ##won't fall below 10
        if epochIter==0:  ###be careful that epochs are actually added by 1
            torch.save(model,saveModelPathPrefix+'/1Epochs/'+trainingCity+'.pth')
        if epochIter==1:
            torch.save(model,saveModelPathPrefix+'/2Epochs/'+trainingCity+'.pth')
        if epochIter==2:
            torch.save(model,saveModelPathPrefix+'/3Epochs/'+trainingCity+'.pth')
        if epochIter==3:
            torch.save(model,saveModelPathPrefix+'/4Epochs/'+trainingCity+'.pth')
        if epochIter==4:
            torch.save(model,saveModelPathPrefix+'/5Epochs/'+trainingCity+'.pth')
        if epochIter==5:
            torch.save(model,saveModelPathPrefix+'/6Epochs/'+trainingCity+'.pth')
        if epochIter==7:
            torch.save(model,saveModelPathPrefix+'/8Epochs/'+trainingCity+'.pth')
        if epochIter==9:
            torch.save(model,saveModelPathPrefix+'/10Epochs/'+trainingCity+'.pth')
            
    
    ###There is an extra zero in loss arrays at beginning
    lossPrimary1Array = lossPrimary1Array[1:-1] 
    lossPrimary2Array = lossPrimary2Array[1:-1] 
    lossSecondary1Array = lossSecondary1Array[1:-1] 
    lossSecondary2Array = lossSecondary2Array[1:-1]        
    
            
    ##Plotting loss
    plt.figure()
    plt.plot(lossPrimary1Array.numpy())
    plt.xlabel('Iteration')
    plt.ylabel('Loss')
    plt.savefig('primaryLoss1Plot.pdf') 
    
    plt.figure()
    plt.plot(lossPrimary2Array.numpy())
    plt.xlabel('Iteration')
    plt.ylabel('Loss')
    plt.savefig('primaryLoss2Plot.pdf') 
    
    
    plt.figure()
    plt.plot(lossSecondary1Array.numpy())
    plt.plot(lossSecondary2Array.numpy())
    plt.legend([r"$\mathcal{L}_{1,2}$",r"$\mathcal{L}'_{1,2}$"])
    plt.xlabel('Iteration')
    plt.ylabel('Loss')
    plt.savefig('secondaryLossesPlot.pdf') 
    
    ##Save the trained model
    saveModelPath = saveModelPathPrefix+'/Final/'+trainingCity+'.pth'
    torch.save(model,saveModelPath)
    ##Save loss values
    
    endTime = time.time()
    elapsedTime = endTime - startTime
    print(elapsedTime)
    x=10/0
    
    
    torch.save({"lossPrimary1Array": lossPrimary1Array, "lossPrimary2Array":lossPrimary2Array,\
                "lossSecondary1Array": lossSecondary1Array, "lossSecondary2Array":lossSecondary2Array}, saveModelPath.replace('.pth','LossValues.pt'))
    
    del model   ##delete so that inadvertently it does not get mixed with another city in training list
    
    
    
    
    







    
    
    
    
    






    



