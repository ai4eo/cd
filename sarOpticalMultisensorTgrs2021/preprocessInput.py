#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 11 20:05:36 2019

@author: sudipan
"""

import os
import numpy as np
import tifffile
import glob
from osgeo import gdal

def saturateSomePercentile(inputMap,percentileToSaturate):
    inputMapNormalized=(inputMap-np.amin(inputMap))/(np.percentile(inputMap,(100-percentileToSaturate))-np.amin(inputMap))
    inputMapNormalized[inputMapNormalized>1]=1
    return inputMapNormalized


def saturateSomePercentileForSentinel2(inputMap,percentileToSaturate):
    inputMapNormalized=(inputMap-np.amin(inputMap))/(np.percentile(inputMap,(100-percentileToSaturate))-np.amin(inputMap))
    inputMapNormalized[inputMapNormalized>1]=1
    return inputMapNormalized


def preprocessSentinel2(inputMap,percentileToSaturate):
    inputMapDividedBy10000 = inputMap/10000
    inputMapDividedBy10000[inputMapDividedBy10000>1]=1
    inputMapNormalized = saturateSomePercentileForSentinel2(inputMapDividedBy10000,percentileToSaturate)
    return inputMapNormalized  


def saturateSomePercentileForSentinel1(inputMap,percentileToSaturate):
    inputMapNormalized=(inputMap-np.amin(inputMap))/(np.percentile(inputMap,(100-percentileToSaturate))-np.amin(inputMap))
    inputMapNormalized[inputMapNormalized>1]=1
    return inputMapNormalized

def preprocessSentinel1(inputImageGdal,percentileToSaturate,requiredShape):
    inputImageAsList = []
    for bandIter in range(1, inputImageGdal.RasterCount +1):
        band = inputImageGdal.GetRasterBand(bandIter)
        inputImageThisChannel = band.ReadAsArray()
        inputImageThisChannel=saturateSomePercentileForSentinel1(inputImageThisChannel,percentileToSaturate)
        inputImageAsList.append(inputImageThisChannel)
   
   
    inputImage = np.array(inputImageAsList)
    ##2 channels for 2 polarizations, however using just 1
    inputImage = inputImage[0,:,:]
    inputImage = np.squeeze(inputImage)
    inputImage=np.repeat(inputImage[:, :, np.newaxis], 3, axis=2) ##copying over 3 channels
    return inputImage

def getImageStatisticsForNormalizing(inputCityName, time1Path, time2Path, whichOrderForPrechangePostchange,topPercentToSaturate):
    if whichOrderForPrechangePostchange=='S2AndS1':
        
        preChangeImageBand1 = tifffile.imread(time1Path+'/imgs_1_rect/'+'B04.tif')        
        preChangeImageBand1 = preprocessSentinel2(preChangeImageBand1,topPercentToSaturate)
        preChangeImageBand2 = tifffile.imread(time1Path+'/imgs_1_rect/'+'B03.tif')
        preChangeImageBand2 = preprocessSentinel2(preChangeImageBand2,topPercentToSaturate)
        preChangeImageBand3 = tifffile.imread(time1Path+'/imgs_1_rect/'+'B02.tif')        
        preChangeImageBand3 = preprocessSentinel2(preChangeImageBand3,topPercentToSaturate)
        
        
        S1PostChangeFilePath = glob.glob(os.path.join(time2Path+'/imgs_2/transformed/','*.tif'))
        S1PostChangeFilePath =  S1PostChangeFilePath[0]
        postChangeImageGdal = gdal.Open(S1PostChangeFilePath, gdal.GA_ReadOnly)
        postChangeImageBand1 = preprocessSentinel1(postChangeImageGdal,topPercentToSaturate,preChangeImageBand1.shape)
        postChangeImageBand2 = np.copy(postChangeImageBand1)
        postChangeImageBand3 = np.copy(postChangeImageBand2)
        
        
    elif whichOrderForPrechangePostchange=='S1AndS2':
        
        postChangeImageBand1 = tifffile.imread(time2Path+'/imgs_2_rect/'+'B04.tif')
        postChangeImageBand1 = preprocessSentinel2(postChangeImageBand1,topPercentToSaturate)
        postChangeImageBand2 = tifffile.imread(time2Path+'/imgs_2_rect/'+'B03.tif')
        postChangeImageBand2 = preprocessSentinel2(postChangeImageBand2,topPercentToSaturate)
        postChangeImageBand3 = tifffile.imread(time2Path+'/imgs_2_rect/'+'B02.tif')
        postChangeImageBand3 = preprocessSentinel2(postChangeImageBand3,topPercentToSaturate)
        
        S1PreChangeFilePath = glob.glob(os.path.join(time1Path+'/imgs_1/transformed/','*.tif'))
        S1PreChangeFilePath =  S1PreChangeFilePath[0]
        preChangeImageGdal = gdal.Open(S1PreChangeFilePath, gdal.GA_ReadOnly) 
        postChangeImageBand1 = preprocessSentinel1(preChangeImageGdal,topPercentToSaturate,postChangeImageBand1.shape)
        preChangeImageBand2 = np.copy(preChangeImageBand1)
        preChangeImageBand3 = np.copy(preChangeImageBand2)
       
        
        
        
    mean1 = (np.mean(preChangeImageBand1), np.mean(preChangeImageBand2),np.mean(preChangeImageBand3))
    std1 = (np.std(preChangeImageBand1), np.std(preChangeImageBand2),np.std(preChangeImageBand3))
    mean2 = (np.mean(postChangeImageBand1), np.mean(postChangeImageBand2),np.mean(postChangeImageBand3))
    std2 = (np.std(postChangeImageBand1), np.std(postChangeImageBand2),np.std(postChangeImageBand3))
        
    return mean1,std1,mean2,std2