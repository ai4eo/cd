# Self-supervised multi-sensor change detection

Code related to https://arxiv.org/pdf/2103.05102.pdf.

trainOverSingleCity.py: trains the self-supervised network from a paired unlabeled optical-SAR scene.

The trained network can be used along with deep change vector analysis (DCVA) for change detection.

