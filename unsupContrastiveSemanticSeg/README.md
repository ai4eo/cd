# Unsupervised Single-Scene Semantic Segmentation for Earth Observation

This work proposes a model for single scene unsupervised semantic segmentation. In addition to semantic segmentation, it can also be used for changed object detection for some applications (by segmenting the post-change image). 

Code is available here: https://github.com/sudipansaha/singleSceneSemSegTgrs2022


